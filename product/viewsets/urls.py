from django.conf.urls import url, include
from rest_framework import routers
from .product_viewset import ProductViewset 
from .category_viewset import CategoryViewset 


router = routers.DefaultRouter()

router.register(r'product', ProductViewset)
router.register(r'category', CategoryViewset)

urlpatterns = [
    url(r'^', include(router.urls)),
]