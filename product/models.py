from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=300)
    category = models.ForeignKey('Category', on_delete=models.PROTECT, related_name='products')
    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name